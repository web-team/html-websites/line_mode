![](screenshot.png)

# Line Mode Browser
  
> The line-mode browser, launched in 1992, was the first readily accessible browser for what we now know as the world wide web. It was not, however, the world’s first web browser. The very first web browser was called WorldWideWeb and was created by Tim Berners-Lee in 1990.

This repository holds the code for CERN's Line Mode Browser simulation. 

The live website is found at: https://line-mode.cern.ch/

It is being served via PaaS at: https://paas.cern.ch/k8s/ns/line-mode/build.openshift.io~v1~BuildConfig

# Requirements

- NodeJS

# Install

1. Clone this repository
2. Run `node .`
3. Open http://localhost:8000/ in your browser

# Contribute

Add your HTML, CSS, and JavaScript to `./public`. You can make a proxy call by requesting anything at `./proxy?url=http://example.com`.

